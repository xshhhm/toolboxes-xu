WU-SVM
======
Version: 0.1 Alpha (March 2014)
------------------------------

```
[Compiled on OSX 10.9 with gcc-4.4 (see build.sh), Chunhua Sept 2014]
```


Please contact Stephen Tyree (swtyree@wustl.edu), Gabriel Hope (ghope@wustl.edu) or Nicholas Kolkin (n.kolkin@wustl.edu) with any questions, feedback, or bug reports.



This tool is meant to be an easy to use and very fast SVM solver that leverages
multicore processors and/or the parallel capabilities of GPU's to create accurate
models in a fraction of the time required by traditional implementations. It
relies on external BLAS/CUBLAS libraries, so it's speed will improve organically
as hardware and BLAS implementations become more advanced.

//////////////////////////////////////////
//            REQUIREMENTS:             //
//////////////////////////////////////////

  Basic Multicore:
    1. CMake (Version 2.8 or higher): <http://www.cmake.org/cmake/resources/software.html>
    2. BLAS/LAPACK Library

        Download Sources:

        Standard: http://www.netlib.org/lapack/#_software

        (faster better optimized implementation, will greatly improve speed)
        ACML: http://developer.amd.com/tools-and-sdks/cpu-development/amd-core-math-library-acml/acml-downloads-resources/
        MKL: http://software.intel.com/en-us/intel-mkl

  Additional Requirements for GPU Version:
    1. Nvidia GPU
    2. CUDA (version 5.0 or later): https://developer.nvidia.com/cuda-downloads


///////////////////////////////////////
//          INSTALLATION:            //
///////////////////////////////////////

  (0) Install requirements
     (a) Install CMake
     (b) Install Blas/Lapack Library
     (c) (GPU version only) Install CUDA (also make sure CUDA drivers are installed for your GPU)

  (1) Navigate to directory containing build.sh (included in download)

  (2) run build.sh (our wrapper for running CMake) or use CMake directly on the src director

  (2.5) Troubleshooting 'build.sh' in text editor

      (a) set 'CMAKE_PATH' equal to the path to CMake (often unnescessary if CMake is installed correctly)
      	  e.g 'CMAKE_PATH =../tools/cmake-2.8.12.1/bin/cmake'

      (b) set 'SRC_PATH' equal to the path where you've stored the source code
      	  e.g 'SRC_PATH=../lasp_release_0.1/src/'

      (c) (GPU version only) set 'CUDA_BIN_PATH' equal to the path to your cuda installation
      	  e.g 'CUDA_BIN_PATH=/usr/local/cuda-5.5'

      (d) Additional options for building/installation are detailed in 'build.sh' (e.g. OS specific options)


  (3) run demo.sh to ensure SP_SVM has correctly installed
      you should be able to see the SVM create the model,
      and the it should get an accuracy on the test set of
      around 85%


///////////////////////////////////////
//          USAGE TRAIN_MC:          //
///////////////////////////////////////

Usage: train_mc [options] [training data file] [model output file]

options:

-k kernel: set type of kernel (default = RBF)
         0 -- Radial Basis Function: exp(-gamma*|u-v|^2)
         1 -- Linear: u'*v
         2 -- Polynomial: (gamma*u'*v + coef)^degree
         3 -- Sigmoid: tanh(gamma*u'*v + coef)

-r coef: sets coefficient value used in some kernel functions (default = 0)

-d degree: sets degree value uesd in some kernel functions (default = 3)

-g gamma: sets gamma value used in some kernel functions (default = 1 / # of features)

-c C: sets c parameter for SVM (default = 1)

-n nb_cand: sets nb_cand (default = 10)

     This sets the number of training example from which the each support vector is chosen.
     For example if nb_cand=10, the the SVM chooses the most valuable support vector from
     examples 1-10, then the next one from example 11-20, the next from 21-30, and so on

-s set_size: sets size (default = 5000)

     This sets the maximum number of support vectors the SVM is allowed to train to

-i max_iter: sets max number of backtracking iterations(default = 20)

     If a newton step is too large, controls the maximum amount of backtracking allowed,
     if you are seeing divergence one option is to try increasing this value

-v verbosity: sets output level (default = 1)

   -v 0: no output
   -v 1: progress indicator
   -v 2: training set updates only
   -v 3: full output

-m max_new_basis: sets maximum number of vectors in new basis (default = 800)

       sets the maximum number of new basis vectors chosen before completely retraining the model

-x stopping_criterion: sets the stopping criterion (default = 5e-6)

       the stopping criterion is the number of new points classified correctly, divided by the
       total number of training points. The SVM stops training once it's improvement is below
       the threshold set with this parameter

-j jump_set: sets the size of the starting training set (default = 100)

       We initialize training with moderate number of basis vectors chosen (default 100), so that
       we start with a feasibly complex model for most problems (rather than wasting time computing
       overly simple ones), the number of basis vectors we start with can be set with this parameter

-b probability_estimates: use Platt scaling to produce probability estimates (default 0)

--gpu (-u) gpu: uses CUDA to accelerate computation

--omp_threads (-T) Open MP threads: sets the maximum number of openMP threads allowed (only used if built openMP)

--no_cache (-K) no cache: avoid caching the full kernel matrix

       Significantly reduces the amount of memory used at the expense of slower training speed

--random (-f) randomize: randomizes training set selection

       Can speed training, potentially at the expense of accuracy (speeds up training if heuristically
       choosing basis vectors is the bottleneck)

--version (-q) version: displays version number and exits

-h help: displays this message


///////////////////////////////////////
//          USAGE CLASSIFY_MC:          //
///////////////////////////////////////

Three arguments required.

usage: [options] ./classify_mc dataFile modelFile outputFile

options:

  -v verbosity: sets output level (default = 1)
     -v 0: no output
     -v 1: prints accuracy

  --gpu (-u) gpu: uses CUDA to accelerate computation

  --dag (-d) dag: use dag model for multiclass classification

  --version (-q) version: displays version number and exits

  -h help: displays this message



/////////////////////////////////////////
//              EXAMPLE:               //
/////////////////////////////////////////

  ./train_mc --gpu -v 3 -s 2000 -c 1.0 -g 0.05 adult.test adult.train adult.model

  trains a classifier on the gpu with verbosity level 3, with a maximum of 2000 support vectors, using an
  RBF kernel (by default) with c = 1.0 and gamma = 0.05

  ./classify_mc adult.test adult.model adult.labels

  tests the classifier trained above and outputs the labels


/////////////////////////////////////////
//            DATA FORMAT:             //
/////////////////////////////////////////

  Input and output is in lib-SVM compatible format. See Lib-SVM website for more details
  on format specifications.

  http://www.csie.ntu.edu.tw/~cjlin/libsvm/


/////////////////////////////////////////
//             CITATIONS:              //
/////////////////////////////////////////

If you use WU-SVM in a published work, please cite the following technical report:

@article{tyree2014parallel,
  title={Parallel Support Vector Machines in Practice},
  author={Tyree, Stephen and Gardner, Jacob R. and Weinberger, Kilian Q. and Agrawal, Kunal and Tran, John},
  journal={arXiv preprint arXiv:1404.1066},
  year={2014}
}

//////////////////////////////////////////
//               LICENSE:               //
//////////////////////////////////////////

Copyright (c) 2014, Washington University in St. Louis
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of Washington University in St. Louis nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL WASHINGTON UNIVERSITY BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
