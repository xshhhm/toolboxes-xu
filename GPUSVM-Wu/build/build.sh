#!/bin/bash

##Build.sh
##Uncomment and set the appropriate variable if needed for your system
##Note: Any values not set will revert to CMake defaults

CPP_COMPILER=/usr/local/bin/g++-4.4
C_COMPILER=/usr/local/bin/gcc-4.4




##CMake executable to use
CMAKE_PATH=cmake

##Path to source code
SOURCE_PATH=../src

##Install executables
INSTALL=false
#INSTALL_PATH=

##C/C++ compilers to use (See CUDA documentation for supported compilers)
#CPP_COMPILER=g++
#C_COMPILER=gcc

##Path to CUDA installation
USE_CUDA=true
#CUDA_PATH=/usr/local/cuda-5.5

##Option to use OpenMP multithreading
USE_OPENMP=true

##Desired blas/lapack library information, set vendor and library path
## for non-standard installations
##
##List of valid vendor options supported by CMake (from FindBLAS.cmake):
##	Goto,ATLAS PhiPACK,CXML,DXML,SunPerf,SCSL,SGIMATH,IBMESSL,Intel10_32
##	(intel mkl v10 32 bit),Intel10_64lp (intel mkl v10 64 bit,lp thread model, lp64 model),
##  Intel10_64lp_seq (intel mkl v10 64 bit,sequential code, lp64 model),
## 	Intel( older versions of mkl 32 and 64 bit), ACML,ACML_MP,ACML_GPU,Apple, NAS, Generic
##
#BLAS_VENDOR=All
#LIBRARY_PATH=""

##Option to do a debug build which may provide useful information if
##	the program does not work correctly
DEBUG_BUILD=false


#####################
  ## RUN SCRIPT ##
#####################
CMAKE_OPTIONS=""

if [ -n "$CPP_COMPILER" ] ; then
    CMAKE_OPTIONS+=" -DCMAKE_CXX_COMPILER=$CPP_COMPILER"
    CMAKE_OPTIONS+=" -DCUDA_HOST_COMPILER=$CPP_COMPILER"
fi

if [ -n "$C_COMPILER" ] ; then
    CMAKE_OPTIONS+=" -DCMAKE_C_COMPILER=$C_COMPILER"
fi

if [ -n "$CUDA_PATH" ] ; then
    CMAKE_OPTIONS+=" -DCUDA_TOOLKIT_ROOT_DIR=$CUDA_PATH"
fi

if [ -n "$BLAS_VENDOR" ] ; then
    CMAKE_OPTIONS+=" -DBLA_VENDOR=$BLAS_VENDOR"
fi

if [ -n "$BLAS_VENDOR" ] ; then
    CMAKE_OPTIONS+=" -DBLA_VENDOR=$BLAS_VENDOR"
fi

if [ -n "$LIBRARY_PATH" ] ; then
    CMAKE_OPTIONS+=" -DCMAKE_LIBRARY_PATH=$LIBRARY_PATH"
fi

if [ -n "$INSTALL_PATH" ] ; then
    CMAKE_OPTIONS+=" -DCMAKE_INSTALL_PREFIX=$INSTALL_PATH"
fi

if [ $USE_CUDA = true ] ; then
    CMAKE_OPTIONS+=" -DUSE_CUDA=ON"
else
    CMAKE_OPTIONS+=" -DUSE_CUDA=OFF"
fi

if [ $USE_OPENMP = true ] ; then
    CMAKE_OPTIONS+=" -DUSE_OMP=ON"
else
    CMAKE_OPTIONS+=" -DUSE_OMP=OFF"
fi

if [ $DEBUG_BUILD = true ] ; then
    CMAKE_OPTIONS+=" -DDEBUG_MODE=ON"
else
    CMAKE_OPTIONS+=" -DDEBUG_MODE=OFF"
fi

$CMAKE_PATH $CMAKE_OPTIONS $SOURCE_PATH
make

if [ $INSTALL = true ] ; then
    make install
fi
