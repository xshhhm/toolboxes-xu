/*
Copyright (c) 2014, Washington University in St. Louis
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Washington University in St. Louis nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
ARE DISCLAIMED. IN NO EVENT SHALL WASHINGTON UNIVERSITY BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; 
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND 
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "parsing.h"
#include "fileIO.h"
#include "pegasos.h"

int main(int argc, char* argv[]){
	lasp::opt options;
	lasp::svm_time_recorder recorder;
	lasp::svm_sparse_data myData;
	if(lasp::parse_and_load(argc, argv, options, myData) != lasp::CORRECT){
		return 1;
	}
	
	vector<lasp::svm_problem> solvedProblems;
	//tracks holdout data, which is used for platt scaling later.
	vector<lasp::svm_sparse_data> holdouts;
	
	for(int i = 0; i < myData.orderSeen.size(); ++i) {
		for(int j = i+1; j < myData.orderSeen.size(); ++j) {
			int firstClass = myData.orderSeen[i];
			int secondClass = myData.orderSeen[j];
			
			if(options.verb > 0) {
				cout << "Training " << firstClass << " v. " << secondClass << " classifier" << endl;
			}
			
			if(options.plattScale) {
				lasp::svm_sparse_data holdoutData;
				lasp::svm_problem curProblem = lasp::get_onevsone_subproblem(myData,
																			 holdoutData,
																			 firstClass,
																			 secondClass,
																			 options);
				if (options.single) {
					if (options.pegasos) {
						lasp::pegasos_svm_host<float>(curProblem);
					} else {
						lasp::lasp_svm_host<float>(curProblem);
					}
					
				} else {
					if (options.pegasos) {
						lasp::pegasos_svm_host<double>(curProblem);
					} else {
						lasp::lasp_svm_host<double>(curProblem);
					}
				}
				
				solvedProblems.push_back(curProblem);
				holdouts.push_back(holdoutData);
			}
			else {
				lasp::svm_problem curProblem = lasp::get_onevsone_subproblem(myData,
																			 firstClass,
																			 secondClass,
																			 options);
				if (options.single) {
					if (options.pegasos) {
						lasp::pegasos_svm_host<float>(curProblem);
					} else {
						lasp::lasp_svm_host<float>(curProblem);
					}
					
				} else {
					if (options.pegasos) {
						lasp::pegasos_svm_host<double>(curProblem);
					} else {
						lasp::lasp_svm_host<double>(curProblem);
					}
				}
				
				solvedProblems.push_back(curProblem);
			}
		}
	}
	
	lasp::svm_model myModel = lasp::get_model_from_solved_problems(solvedProblems, holdouts, myData.orderSeen);
	lasp::write_model(myModel, options.modelFile);
}
